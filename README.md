# Food Order App v2 - React App (Platform Electives Objective)
Improving the Food Order App v1, it has mostly the same functionalities but this time, dynamic data from the backend is used rather than static data. Firebase database is used as the main backend of the application. Another additional component is the user order form wherein details of the user are needed in completing the order transaction. It is then written back to the backed as a specific order as data. In all, the core objectives that were used in this app was implemented from the previous sections such as HTTP Requests, and Form Validation.

## Setup

```
npm install
npm run start
```
